const path = require('path')
const glob = require('glob')

const {
  publicDir,
  legacyBundleName,
  manifestName
} = require('../config.json')

const {
  writeCSSAsset,
  writeJSAsset,
  writeLibraries,
  writeInfo
} = require('../utils/writeAssets')

class ModernBuildPlugin {
  apply(compiler) {
    const pluginName = 'modern-build-plugin';

    // Получаем информацию о Fallback Build
    const legacyManifest = require(path.resolve(publicDir, legacyBundleName, manifestName));

    compiler.hooks.compilation.tap(pluginName, (compilation) => {
      // Подписываемся на хук html-webpack-plugin,
      // в котором можно менять данные HTML
      compilation.hooks.htmlWebpackPluginAlterAssetTags.tapAsync(pluginName, (data, cb) => {

        data.head.forEach((tag) => {
          if (tag.tagName === 'link' && tag.attributes.rel === 'stylesheet') {
            writeCSSAsset(tag.attributes.href)
          }
        })

        data.body.forEach((tag) => {
          if (tag.tagName === 'script' && tag.attributes) {
            data.head.push({
              tagName: 'script',
              closeTag: true,
              attributes: {
                type: 'module',
                src: tag.attributes.src
              }
            })

            writeJSAsset(tag.attributes.src, true)

            let src = path.parse(tag.attributes.src)
            let file = src.base
            src = legacyManifest[file]

            tag.attributes.nomodule = true
            tag.attributes.defer = true
            tag.attributes.src = src
            delete tag.attributes.type

            writeJSAsset(tag.attributes.src, false)
          }
        });

        if (
          glob.sync(path.resolve('*.libraries.yml'))[0]
          && glob.sync(path.resolve('*.info.yml'))[0]
        ) {
          writeLibraries();
          writeInfo();
        }

        cb();
      });
    });
  }
}

module.exports = ModernBuildPlugin;
