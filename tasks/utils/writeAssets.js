const yaml = require('js-yaml')
const fs = require('fs')
const path = require('path')
const glob = require('glob')

const {
  appStylesName,
  appModernScriptsName,
  appLegacyScriptsName
} = require('../config.json')


const appStyles = {
  css: {
    component: {}
  }
}

const appModernScripts = {
  header: true,
  js: {},
  dependencies: [
    'core/jquery',
    'core/drupal'
  ]
}

const appLegacyScripts = {
  js: {},
  dependencies: [
    'core/jquery',
    'core/drupal'
  ]
}

let newLibraries = []

const writeCSSAsset = (href) => {
  appStyles.css.component[href] = {}
}

const writeJSAsset = (src, modern = false) => {
  modern
    ? appModernScripts.js[src] = { attributes: { type: 'module' } }
    : appLegacyScripts.js[src] = { attributes: { nomodule: true, defer: true } }
}

const writeLibraries = () => {
  const librariesPath = glob.sync(path.resolve('*.libraries.yml'))[0]
  const libraries = yaml.safeLoad(fs.readFileSync(librariesPath, 'utf8')) || {}
  const dirAbsolute = path.dirname(librariesPath).split(path.sep)
  const dir = dirAbsolute[dirAbsolute.length - 1]

  newLibraries = []

  libraries[appStylesName] = appStyles
  newLibraries.push(path.join(dir, appStylesName))

  libraries[appModernScriptsName] = appModernScripts
  newLibraries.push(path.join(dir, appModernScriptsName))

  libraries[appLegacyScriptsName] = appLegacyScripts
  newLibraries.push(path.join(dir, appLegacyScriptsName))

  const ws = fs.createWriteStream(librariesPath)
  const modifiedLabraries = yaml.safeDump(libraries, {
    condenseFlow: true
  })
  ws.end(modifiedLabraries)
}

const writeInfo = () => {
  const infoPath = glob.sync(path.resolve('*.info.yml'))[0]
  const info = yaml.safeLoad(fs.readFileSync(infoPath, 'utf8')) || {}
  const oldLibraries = info.libraries || []

  newLibraries.forEach((libriary) => {
    if (oldLibraries.includes(libriary)) return
    oldLibraries.push(libriary)
  })

  info.libraries = oldLibraries

  const ws = fs.createWriteStream(infoPath)
  const modifiedInfo = yaml.safeDump(info, {
    condenseFlow: true
  })
  ws.end(modifiedInfo)
}

module.exports = {
  writeCSSAsset,
  writeJSAsset,
  writeLibraries,
  writeInfo
}