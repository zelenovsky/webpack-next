const isModern = process.env.BROWSERS_ENV === 'modern';

module.exports = () => {
  return {
    test: /\.m?js$/,
    use: {
      loader: 'babel-loader',
      options: {
        babelrc: false,
        presets: [
          ['@babel/preset-env', {
            debug: false,
            modules: false,
            useBuiltIns: 'entry',
            targets: isModern ? { esmodules: true } : undefined,
          }],
        ],
        plugins: ['@babel/plugin-syntax-dynamic-import'],
      },
    },
  };
};