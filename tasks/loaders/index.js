const babelLoader = require('./babelLoader');
const styleLoader = require('./styleLoader');
const htmlLoader = require('./htmlLoader');


module.exports = {
  babelLoader,
  styleLoader,
  htmlLoader
}