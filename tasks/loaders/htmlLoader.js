module.exports = () => {
  return {
    test: /\.pug$/,
    use: [
      {
        loader: 'html-loader',
      },
      {
        loader: 'pug-html-loader'
      }
    ]
  };
};