const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

const getFilePath = require('./utils/getFilePath');
const {
  styleLoader,
  babelLoader,
  htmlLoader
} = require('./loaders');
const plugins = require('./plugins');
const {
  publicDir
} = require('./config.json');

const isModern = process.env.BROWSERS_ENV === 'modern';
const isProd = process.env.NODE_ENV === 'production';


const configureEntry = () => {
  return {
    'app': isModern ? './app/app.js' : './app/app-legacy.js',
  }
}

const configureOutput = () => {
  return {
    path: path.resolve(publicDir),
    filename: isProd
      ? getFilePath('js/[name].[chunkhash].js')
      : getFilePath('js/[name].js'),
    // chunkFilename: isProd
    //   ? getFilePath('js/chunks/[name].[chunkhash].js')
    //   : getFilePath('js/chunks/[name].js')
  }
}


module.exports = {
  entry: configureEntry(),
  output: configureOutput(),
  mode: isProd ? 'production' : 'development',
  watch: !isProd && isModern ? true : false,
  cache: {},
  devtool: isProd ? 'source-map' : 'inline-source-map',
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all'
        }
      }
    },
    minimizer: [
      new TerserPlugin({
        test: /\.m?js$/,
        sourceMap: true,
        terserOptions: {
          mangle: {
            properties: /(^_|_$)/,
          },
          safari10: true,
        },
      }),
    ],
  },
  module: {
    rules: [
      babelLoader(),
      styleLoader(),
      htmlLoader()
    ],
  },
  plugins: plugins()
}
